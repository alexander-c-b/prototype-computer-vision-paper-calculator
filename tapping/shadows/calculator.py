# %%
from collections.abc import Generator, Iterable
import enum
from dataclasses import dataclass
import operator
import sys
from typing import Optional

import numpy as np
from numpy.typing import NDArray


class OperationType(enum.Enum):
    PLUS = operator.add
    MINUS = operator.sub
    TIMES = operator.mul
    DIV = operator.floordiv


@dataclass
class Digit:
    digit: int

    def add_in_ones_place(self, int):
        return 10 * int + self.digit


@dataclass
class Operation:
    operation: OperationType

    def perform(self, first: int, second: int) -> int:
        return self.operation.value(first, second)

    def __str__(self) -> str:
        match self.operation:
            case OperationType.PLUS:
                return "+"
            case OperationType.MINUS:
                return "-"
            case OperationType.TIMES:
                return "×"
            case OperationType.DIV:
                return "÷"


@dataclass
class Equals:
    pass


CalculatorButton = Digit | Operation | Equals


def map_color(pixel: NDArray[np.uint8]) -> Optional[CalculatorButton]:
    match tuple(pixel[::-1]):
        case (0, 78, 255):
            return Digit(0)
        case (42, 0, 255):
            return Digit(1)
        case (105, 0, 255):
            return Digit(2)
        case (169, 0, 255):
            return Digit(3)
        case (0, 154, 255):
            return Digit(4)
        case (0, 211, 255):
            return Digit(5)
        case (0, 249, 255):
            return Digit(6)
        case (0, 255, 211):
            return Digit(7)
        case (0, 255, 148):
            return Digit(8)
        case (23, 255, 0):
            return Digit(9)
        case (162, 255, 0):
            return Operation(OperationType.PLUS)
        case (225, 255, 0):
            return Operation(OperationType.MINUS)
        case (255, 221, 0):
            return Operation(OperationType.TIMES)
        case (255, 177, 0):
            return Operation(OperationType.DIV)
        case (255, 0, 171):
            return Equals()
        case _:
            return None


def clear_line() -> None:
    print("\r" + " " * 30 + "\r", end="")


def parse_and_print(buttons: Iterable[CalculatorButton]) -> None:
    previous_number = 0
    current_operation = Operation(OperationType.PLUS)
    current_number = 0
    for button in buttons:
        match button:
            case Digit():
                print(button.digit, end="")
                current_number = button.add_in_ones_place(current_number)
            case Operation():
                previous_number = current_operation.perform(
                    previous_number, current_number
                )
                current_operation = button
                current_number = 0
                print(f" {button} ", end="")
            case Equals():
                result = current_operation.perform(previous_number, current_number)
                print(f"\n= {result}\n", end="\n\n")
                previous_number = 0
                current_operation = Operation(OperationType.PLUS)
                current_number = 0
        sys.stdout.flush()


# %%
from time import sleep
from typing import TypeVar

trial: list[CalculatorButton] = [
    Digit(1),
    Digit(2),
    Digit(6),
    Operation(OperationType.DIV),
    Digit(3),
    Equals(),
]

T = TypeVar("T")


def sleep_between(iterable: Iterable[T]) -> Iterable[T]:
    for item in iterable:
        sleep(0.5)
        yield item
