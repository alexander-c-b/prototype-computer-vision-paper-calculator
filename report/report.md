-   introduction

    -   applications in medicine - robust tap detection allows for extremely
        sanitary human-computer interfaces - a sheet of paper with input options
        can be printed for a low cost - then thrown away - doesn't need to be
        carefully cleaned for the next user - platform for low-cost assistive
        technologies using hardware consumers already have - customizeable to
        fit an individual's specific needs
    -   no apparent methods for detecting taps or finger touches based on
        contemporary video cameras such as those found in smartphones
    -   paper presents a method for detecting finger touches on a flat surface
        based on fingertip tracking and shadow segmentation
        -   example application: paper four-function calculator
        -   illustrate the functionality and potential limitations of the method

-   Background (related computer vision methodologies/approaches)

    -   finger recognition
        -   segmentation
            -   useful for a proof of concept
            -   major limitations
                -   a robust detection method must account for differences in
                    skin color and variations in color, saturation and intensity
                    across one's hand
                -   no direct information about the locations of the fingertips,
                    only the arm and hand as a whole
            -   one improvement could be
                -   detect tips based on the curvature or parabolic shape of the
                    fingertip, which is one of the most curved parts of the
                    segmented image
                -   requires robust segmentation
        -   hand landmark detection
            -   Google's MediaPipe library can effectively identify fingertip
                locations and other hand "landmarks"---i.e. finger joints,
                knuckles and the wrist---
            -   however, detection fails altogether when enough landmarks move
                out of view
                -   fails often when holding a pointing gesture
        -   tip detection
            -   no prior research identified which could reliably identify
                fingertips even when some were occluded
    -   shadow detection
        -   mostly focused on shadow removal
            -   shadows are integral to this method, so removal is contrary to
                this work's purpose
        -   further work is needed to identify if there are existing methods or
            parts thereof to segment and/or track shadows
    -   depth recognition
        -   intel midas
        -   pydnet
        -   others
            -   google
            -   facebook
        -   single-image (not video) has serious shortcomings for this work
            -   can't settle on a consistent reference depth for the background
            -   MiDaS also struggled to provide accurate and consistent
                estimates of the difference in depth between the fingertip and
                surrounding paper
    -   PyTouch
        -   focused on sensor-based touch detection, not computer vision
        -   some sensors internally use computer vision, but don't allow users
            to use existing (smartphone) cameras to identify touch

-   proposed methodology

    -   several major but unrealistic simplifications made
        -   to allow this paper to focus on the core functionality of
            shadow-segmentation-based tap detection
        -   uses recorded video of the calculator interaction
            -   any real application must be real-time
        -   paper sheet placed on a green background
            -   green: very different hue from skin color and shadows
            -   simplifies hand segmentation
        -   video manually perspective-warped to match the calculator's
            orientation
            -   using OpenCV's `warpPerspective` function
            -   the rectangular green background also simplified perspective
                warping
        -   button zones on the paper calculator were manually specified for the
            particular input video
            -   in a real application, the buttons on the paper calculator
                should be automatically identified
        -   the range of hues used for hand segmentation is manually specified
            according to the range of readings from the author's own hand, which
            will not be consistent for users with different skin tone and color
        -   assumed that the user always points with a single finger
            -   the system doesn't identify multiple fingers acting
                independently and has no way of differentiating between fingers
                if multiple are used

    Original image starting point: @fig:original-example

    For each frame:

    1.  Threshold the image to include pixels with hues in the range of skin
        hues

        -   hues: 14° to 48°
            -   correspond to red, orange and yellow hues
            -   cite hue-saturation-value definition/description
        -   figure
            -   binary image
                -   include target reticule for step 2
                -   @fig:thresholded-reticule
            -   hue image
                -   bgr → hsv → saturation & intensity constant → bgr
                -   @fig:hue

    2.  Identify the fingertip as the topmost pixel of value 1 in the binary
        segmented image

        -   see @fig:thresholded-reticule

    3.  apply a gaussian blur (15×15 window size with σ = 8), then Apply
        background subtraction \[cite opencv tutorial\] to obtain a trinary
        image denoting background, foreground and shadow pixels

        -   figure
        -   @fig:gaussian-blur

    4.  Centering at the fingertip, find a 151×151 window on the thresholded
        hand image, and a 151×151 window on the background-subtraction result.

        -   figures
        -   @fig:finger-segmentation-window

    5.  Dilate the pixels in the window from the hand-segmentation image; all
        resulting pixels are *not* considered for shadow detection to avoid
        interference from dark parts of the finger

    6.  After removing shadow pixels which overlap with the now-dilated hand
        segment, count the remaining shadow pixels.

    7.  If the count of shadow pixels is over 1200, then this is counted as a
        "touch" frame; however, this does not necessarily mean that the frame is
        counted as a touch.

        a.  Instead, if at least three of the last 10 frames have been touch
            frames as of the current touch frame, and there wasn't an ongoing
            touch event, then we consider this the beginning of a touch event.

        b.  A touch event ends only when there have been two or fewer touch
            frames in the last 10 frames.

    8.  Once a touch event has occured, the pixel location of the detected
        fingertip is used to index a segmented image of the calculator button
        zones

        -   figure
        -   @fig:calc-colored

    9.  Finally, the detected button presses are streamed to a simple terminal
        calculator application which calculates and prints results as calculator
        operations are detected.

# Results and Analysis

Overall, the touch detection system is accurate enough to serve as a
demonstration, although significant inaccuracy and inprecision are present.
\*@tbl:venn-results displays a breakdown of the system's accuracy for the sample
video, describing correct and incorrect labels for the touch events in the
video. As +@tbl:venn-results also shows, some touch events were detected but the
incorrect operation was recorded; in practice, this was usually the result of
the rudimentiary fingertip tracker identifying the wrong pixel for the fingertip
at the time of the touch event.

@fig:progression shows the qualitative results of the system. As each touch
event in the video occurs, the calculator is updated in real time with the
result of the operation.

# Conclusion and Future Work

This project implemented a touch-identification system through identification of
strong shadows near the user's fingertip, and demonstrated the potential of the
solution through a paper calculator application. Results show that the overall
method has promise for future applications, but there are issues to resolve
regarding the accuracy of the method. In particular, future industrial or
research applications of the method will require robust detection free from
mislabeled or undetected touch events. Future work also includes developing
axiliary components of the project to offer real-time, dynamic performance;
specifically, real-time video capture and automatic identification of button
zones are two major, basic requirements for further applications.
